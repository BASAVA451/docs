# Exception Request

## After the 7th

Once the [stable branch is frozen][freeze], the only merge requests that can be
cherry-picked into the stable branch are:

* Fixes for regressions that affect the current release. This is indicated by the label `~regression:x.y` where `x.y` is the current release milestone.
  For example, if the current release is `11.1`, only bug fixes with `~regression:11.1` are allowed to be picked.
  All other cases of regression fixes will need to wait for the next release or go through the [Exception Request](#asking-for-an-exception-request) process.
   * If no `~regression:x.y` label is set, or if the affected version cannot be determined, we err on the side of **NOT** allowing it after feature freeze. It can go through the Exception Request process if needed.
* Fixes for security issues
* Fixes or improvements to automated QA scenarios
* Documentation updates for changes in the same release
* New or updated translations (as long as they do not touch application code)
* Changes to the CI configuration (`.gitlab-ci.yml`)
* Changelog entries

[freeze]: ../release-candidates.md#feature-freeze-rc

## Asking for an exception request

If you think a merge request should go into a release candidate (RC) or patch even though it does not meet these requirements,
you can ask for an exception to be made.

**Do not** set the relevant `Pick into X.Y` label (see above) before requesting
an exception; this should be done after the exception is approved.

Whether an exception is made is determined by weighing the benefit and urgency of the change
(how important it is to the company that this is released right now instead of in a month)
against the potential negative impact
(things breaking without enough time to comfortably find and fix them before the release on the 22nd).
When in doubt, we err on the side of _not_ cherry-picking.

For example, it is likely that an exception will be made for a trivial 1-5 line performance improvement
(e.g., adding a database index or adding eager loading to a query), but not for a new feature, no matter how relatively small or thoroughly tested.

All MRs which have had exceptions granted must be merged by the 15th.

### Developer
If you are a Developer asking for an exception request, check the [developer guide](developer.md)

### Release Manager
If you are a Release Manager, check the [release manager guide](release-manager.md)
